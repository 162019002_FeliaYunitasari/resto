package Model;

public class transaksi {
    private String kode_bill;
    private String kode_transaksi;
    private int id_menu;
    private int kuantitas;

    
    public transaksi(){
        
    }
    
    public transaksi(String kode_bill, String kode_transaksi, int id_menu, int kuantitas){
        this.kode_bill = kode_bill;
        this.kode_transaksi = kode_transaksi;
        this.id_menu = id_menu;
        this.kuantitas = kuantitas;
    }

    public String getKode_bill() {
        return kode_bill;
    }

    public void setKode_bill(String kode_bill) {
        this.kode_bill = kode_bill;
    }

    public String getKode_transaksi() {
        return kode_transaksi;
    }

    public void setKode_transaksi(String kode_transaksi) {
        this.kode_transaksi = kode_transaksi;
    }

    public int getId_menu() {
        return id_menu;
    }

    public void setId_menu(int id_menu) {
        this.id_menu = id_menu;
    }

    public int getKuantitas() {
        return kuantitas;
    }

    public void setKuantitas(int kuantitas) {
        this.kuantitas = kuantitas;
    }

}

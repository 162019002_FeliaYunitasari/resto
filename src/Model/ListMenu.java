package Model;

public class ListMenu {
    private int id_menu;
    private String nama_menu;
    private String jenis;
    private int harga;
    private String tersedia;

    
    public ListMenu(){
        
    }
    
    public ListMenu(int id_menu, String nama_menu, String jenis, 
                              int harga, String tersedia){
        this.id_menu = id_menu;
        this.nama_menu = nama_menu;
        this.jenis = jenis;
        this.harga = harga;
        this.tersedia = tersedia;

    }

    public int getIdMenu() {
        return id_menu;
    }

    public void setIdMenu(int id_menu) {
        this.id_menu = id_menu;
    }

    public String getNamaMenu() {
        return nama_menu;
    }

    public void setNamaMenu(String nama_menu) {
        this.nama_menu = nama_menu;
    }

    public String getJenis() {
        return jenis;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

    public int getHarga() {
        return harga;
    }

    public void setHarga(int harga) {
        this.harga = harga;
    }

    public String getTersedia() {
        return tersedia;
    }

    public void setTersedia(String tersedia) {
        this.tersedia = tersedia;
    }

    
}

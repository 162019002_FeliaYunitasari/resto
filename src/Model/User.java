package Model;

public class User {
    private int id_user;
    private String nama;
    private String email;
    private String role;
    private String password;

    
    public User(){
        
    }
    
    public User(int id_user, String nama, String email, String password, String role){
        this.id_user = id_user;
        this.nama = nama;
        this.email = email;
        this.role = role;
        this.password = password;
    }

    public int getIdUser() {
        return id_user;
    }

    public void setIdUser(int id_user) {
        this.id_user = id_user;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
    
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    
}

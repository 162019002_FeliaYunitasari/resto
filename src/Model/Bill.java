package Model;

public class Bill {
    private String kodeBill;
    private int id_users;
    private String tanggal_transaksi;
    private String nama_customer;
    private String telp_customer;
    private int pembayaran;
    
    private String kodeTransaksi;
    private String namaMenu;
    private String harga;
    private String kuantitas;
    private String totalHarga;

    
    public Bill(){
        
    }
    
    public Bill(String kodeBill, int id_users, String tanggal_transaksi, String nama_customer,
          String telp_customer, int pembayaran){
        this.kodeBill = kodeBill;
        this.id_users = id_users;
        this.tanggal_transaksi = tanggal_transaksi;
        this.nama_customer = nama_customer;
        this.telp_customer = telp_customer;
        this.pembayaran = pembayaran;
    }

    public String getKodeBill() {
        return kodeBill;
    }

    public void setKodeBill(String value) {
        this.kodeBill = value;
    }

    public int getId_users() {
        return id_users;
    }

    public void setId_users(int id_users) {
        this.id_users = id_users;
    }

    public String getTanggal_transaksi() {
        return tanggal_transaksi;
    }

    public void setTanggal_transaksi(String tanggal_transaksi) {
        this.tanggal_transaksi = tanggal_transaksi;
    }

    public String getNama_customer() {
        return nama_customer;
    }

    public void setNama_customer(String nama_customer) {
        this.nama_customer = nama_customer;
    }

    public String getTelp_customer() {
        return telp_customer;
    }

    public void setTelp_customer(String telp_customer) {
        this.telp_customer = telp_customer;
    }

    public int getPembayaran() {
        return pembayaran;
    }

    public void setPembayaran(int pembayaran) {
        this.pembayaran = pembayaran;
    }
    
    public void setKodeTransaksi(String value) {
        this.kodeTransaksi = value;
    }
    public void setNamaMenu(String value) {
        this.namaMenu = value;
    }
    public void setHarga(String value) {
        this.harga = value;
    }
    public void setKuantitas(String value) {
        this.kuantitas = value;
    }
    public void setTotalHarga(String value) {
        this.totalHarga = value;
    }
    public String getKodeTransaksi() {
        return kodeTransaksi;
    }
    public String getNamaMenu() {
        return namaMenu;
    }
    public String getHarga() {
        return harga;
    }
    public String getKuantitas() {
        return kuantitas;
    }
    public String getTotalHarga() {
        return totalHarga;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import  java.sql.Connection;
import  java.sql.ResultSet;
import  java.sql.SQLException;
import  java.sql.Statement;
import  java.util.ArrayList;
import  java.util.List;
import  java.util.Scanner;
import  Model.ListMenu;
import  Model.Bill;
/**
 *
 * @author feli
 */
public class ControllerBill {
    ConnectionManager conMan = new ConnectionManager();
    Connection con = conMan.LogOn();
    public String totalPembayaran(String kodeBill) {
        String result = "0";
        String queryWhere = "SELECT SUM(c.harga * b.kuantitas) as total_pembayaran FROM bill a LEFT JOIN transaksi b ON b.kode_bill = a.kode_bill LEFT JOIN menu c ON c.id_menu = b.id_menu WHERE a.kode_bill = '"+ kodeBill +"' GROUP BY a.kode_bill";
    
        try{
            Statement stm = con.createStatement();
            ResultSet resultQuery = stm.executeQuery(queryWhere);
            
            if (resultQuery.next()) {
                result = resultQuery.getString("total_pembayaran");
            }
        }catch(SQLException ex){
            System.out.println(ex.toString());
        }
        return result;
    }
    public boolean checkBill(String kodeBill ){
        boolean result = false;
        String queryWhere = "SELECT * FROM bill WHERE kode_bill = '" + kodeBill + "'";
        try{
            Statement stm = con.createStatement();
            ResultSet resultQuery = stm.executeQuery(queryWhere);
            
            if (resultQuery.next()) {
                if (kodeBill.equals(resultQuery.getString("kode_bill"))) {
                    result = true;
                }else{
                    System.out.println("GAGAL");
                }
            }
        }catch(SQLException ex){
            System.out.println(ex.toString());
        }
        
        return result;
    }
    public void createBill(String kodeBill, String idUser, String tanggalTransaksi, String namaCustomer, String telpCustomer, int pembayaran){
        String query = "INSERT INTO bill VALUES ";
        query += "('"+kodeBill+"','"+idUser+"','"+tanggalTransaksi+"','"+namaCustomer+"','"+telpCustomer+"','"+pembayaran+"')";
        try{
            Statement st = con.createStatement();
            int result = st.executeUpdate(query);
        }catch(Exception ex){
            System.out.println(ex);
            System.out.println("Input Gagal");
        }
    }
    public void addPesanan(String kodeBill, String kodeTransaksi, String idMenu, String kuantitas){
        String query = "INSERT INTO transaksi VALUES ";
        query += "('"+kodeBill+"','"+kodeTransaksi+"','"+idMenu+"','"+kuantitas+"')";
        try{
            Statement st = con.createStatement();
            int result = st.executeUpdate(query);
        }catch(Exception ex){
            System.out.println(ex);
            System.out.println("Input Gagal");
        }
    }
    
    public  List<Bill>  tampilTransaksi(String kodeBill) {
        List<Bill>  dataTransaksi  =  new  ArrayList();
        try {
            Statement stm = con.createStatement();
            ResultSet rs = stm.executeQuery("SELECT a.*, b.kode_transaksi, c.nama_menu, c.harga, b.kuantitas, SUM(c.harga * b.kuantitas) as total_harga FROM bill a LEFT JOIN transaksi b ON b.kode_bill = a.kode_bill LEFT JOIN menu c ON c.id_menu = b.id_menu WHERE a.kode_bill = '"+kodeBill+"' GROUP BY b.kode_transaksi");
            while(rs.next())
               {
                Bill bill = new Bill();
                bill.setKodeTransaksi(rs.getString("kode_transaksi"));
                bill.setNamaMenu(rs.getString("nama_menu"));
                bill.setHarga(rs.getString("harga"));
                bill.setKuantitas(rs.getString("kuantitas"));
                bill.setTotalHarga(rs.getString("total_harga"));
                dataTransaksi.add(bill);
               }
            }catch  (SQLException  ex)  {
                System.out.println(ex.toString());
            }
        return  dataTransaksi;
    }
    
    
}

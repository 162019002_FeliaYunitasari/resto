package Controller;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class ConnectionManager {
    public Connection con;
    public Statement stm;
    
    private String Driver = "com.mysql.jdbc.Driver";
    private String url = "jdbc:mysql://localhost:3306/resto_feli";
    private String username ="root";
    private String Password = "";
    
        public Connection LogOn(){
        try{
            Class.forName(Driver).newInstance();
            con = DriverManager.getConnection(url, username,Password);
            stm = con.createStatement();
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return con;
    }
    
    public void LogOff(){
        try{
            con.close();
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }               
}
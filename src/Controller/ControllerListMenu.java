package Controller;

import  java.sql.Connection;
import  java.sql.ResultSet;
import  java.sql.SQLException;
import  java.sql.Statement;
import  java.util.ArrayList;
import  java.util.List;
import  java.util.Scanner;
import  Model.ListMenu;
public class ControllerListMenu {
    ConnectionManager conMan = new ConnectionManager();
    Connection con = conMan.LogOn();
    
    public void addData(String namaMenu, String jenis, String harga, Boolean tersedia){
        int booleanAlgebra = tersedia ? 1 : 0;
        String query = "INSERT INTO menu (nama_menu, jenis, harga, tersedia) values ";
        query += "('"+namaMenu+"','"+jenis+"','"+harga+"','"+booleanAlgebra+"')";
        try{
            Statement st = con.createStatement();
            int result = st.executeUpdate(query);
        }catch(Exception ex){
            System.out.println(ex);
            System.out.println("Input Gagal");
        }
    }
    
    public void update(String idMenu, String namaMenu, String jenis, String harga, Boolean tersedia){
        int booleanAlgebra = tersedia ? 1 : 0;
        String query ="UPDATE menu set nama_menu = '" + namaMenu + "', jenis = '" + jenis + "', harga = '" + harga + "', tersedia = '" + booleanAlgebra + "' ";
        query += "WHERE id_menu = '" + idMenu + "'";
        try{
            Statement stm=con.createStatement();
            int result = stm.executeUpdate(query);
        }catch(SQLException ex){
            System.out.println(ex.toString());
        }
    }
    public void delete(String idMenu){
        int result = 0;
        String query ="DELETE FROM menu WHERE id_menu ='" +idMenu +"'";
        try{
            Statement stm=con.createStatement();
            result=stm.executeUpdate(query);
        }catch(SQLException ex){
            System.out.println(ex.toString());
        }

    }
    public  List<ListMenu>  tampil() {
        List<ListMenu>  dataMenu  =  new  ArrayList();
        try {
            Statement  stm  =  con.createStatement();
            ResultSet  rs  =  stm.executeQuery("SELECT * FROM menu ORDER BY nama_menu ASC");
            while(rs.next())
               {
                ListMenu menu = new ListMenu();
                menu.setIdMenu(Integer.parseInt(rs.getString("id_menu")));
                menu.setNamaMenu(rs.getString("nama_menu"));
                menu.setJenis(rs.getString("jenis"));
                menu.setHarga(Integer.parseInt(rs.getString("harga")));
                menu.setTersedia(rs.getString("tersedia"));
                dataMenu.add(menu);
               }
            }catch  (SQLException  ex)  {
                System.out.println(ex.toString());
            }
        return  dataMenu;
    }
    
}
